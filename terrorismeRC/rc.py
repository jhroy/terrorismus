# coding: utf-8
# ©2020 Jean-Hugues Roy. GNU GPL v3.

import csv, spacy

fichIN = "descripteurs.txt"
# fichOUT = "descripteurs.csv"
# fichOUT2 = "descripteurs2.csv"
fichOUT3 = "descripteurs3.csv"

tal = spacy.load("fr_core_news_sm")

f = open(fichIN, "r", encoding="utf-8")
d = f.read()
n = x = 0
descripteurs = d.split("Titre d'émission: ")
# descripteurs = d.split("Diffusion: ")
print(len(descripteurs))

pollution = ["Descr", "sonore", "visuelle", "Visuel", "Archives", "Supports", "Sortie", "Remarques", "Cassette", "Statut", "Définition", "antenne", "vidéo", "Couleur", "descripteur", "Modifié", "Indexé", "par", "le", "A", "BETACAM", "VOUTE", "Élément", "transféré", "de", "la", "GroupeFilm", "MATÉRIEL", "NON", "VISIONNÉ", "Contenu", "Support", "DATE", "INVENTAIRE", "EN" "ENTREVUE", "ANGLAIS", "POS", "DAV", "indexation", "FILM", "NX", "EXT", "GP", "INT", "PG", "DIV", "ARCH", "PAN", "ENTR", "SIL", "PM", "ANALYSE", "NUMÉRIQUE", "NUMERIQUE", "Cinégramme", "Standard", "SD", "HD", "ATTENTION", "SON", "SUR", "PISTES", "JOUR", "ANALOGIQUE", "AUDIO", "IVI", "WIND", "RADIO", "CANADA", "MTLAVPP", "MPEG", "DALET", "ARCPRO", "CD", "R", "CER", "PS", "NUIT", "ZOOM", "IN", "OUT", "POLYCOPY", "JOURNALISTE", "RÉALISATION", "BETAN", "FI", "STAND", "UP"]

for descripteur in descripteurs:
	n += 1
	diff = descripteur.find("Diffusion:")
	date = descripteur[diff+11:diff+22]
	# print(date)
	no = descripteur.find("No émission:")
	noEmission = descripteur[no+13:no+26]
	# print(noEmission)

	if "Résumé" in descripteur:
		res = descripteur.split("Résumé")
		# print(res[1])
		res = res[1].split("Artisans, auteurs, compositeurs du segment")
		# print(res[0])
		doc = tal(res[0])
		# resume = [token.text for token in doc if token.is_alpha == True and token.text not in pollution]
		resume = [token.lemma_ for token in doc if token.is_alpha == True and token.text not in pollution]
		# print(mots)

	elif "Descr. sonore/visuelle" in descripteur:
		res = descripteur.split("sonore/visuelle")
		res = res[1].split("Artisans, auteurs, compositeurs du segment")
		# print(res[0])
		doc = tal(res[0])
		# resume = [token.text for token in doc if token.is_alpha == True and token.text not in pollution]
		resume = [token.lemma_ for token in doc if token.is_alpha == True and token.text not in pollution]

	else:
		# print(descripteur)
		# print("*"*8)
		resume = "Aucun résumé"
		# print(resume)
	# print("*"*8)

	# descripteur = "Titre d'émission: " + descripteur
	# print(descripteur)
	# if "Diffusion:" in descripteur:
	# 	x += 1
	# 	diff = descripteur.find("Diffusion:")
	# 	date = descripteur[diff+11:diff+22]
	# 	print(date,n,x)
	# print(n,date)
	try:
		elements = date.split("-")
		output = [n,noEmission,date,elements[0],elements[1],elements[2],resume]
	except:
		output = [n,noEmission,date,0,0,0,resume]
	print(output)
	print("="*10)	

	magda = open(fichOUT3, "a")
	fusaro = csv.writer(magda)
	fusaro.writerow(output)